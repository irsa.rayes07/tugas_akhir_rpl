-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 21 Jan 2022 pada 08.20
-- Versi server: 10.4.22-MariaDB
-- Versi PHP: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopci`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_brand`
--

CREATE TABLE `tbl_brand` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `brand_description` text NOT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_brand`
--

INSERT INTO `tbl_brand` (`brand_id`, `brand_name`, `brand_description`, `publication_status`) VALUES
(7, 'Indofood', '                                                                        <font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">PT Indofood Sukses Makmur Tbk atau lebih dikenal dengan nama Indofood merupakan produsen berbagai jenis makanan dan minuman yang bermarkas di Jakarta, Indonesia</span></font>', 1),
(8, 'ABC', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">PT ABC President Indonesia adalah perusahaan makanan dan minuman dari Indonesia yang berbasis di Jakarta yang merupakan perusahaan patungan antara ABC Holding dan Uni-President Enterprises Corporation dari Taiwan.</span></font>', 1),
(9, 'Sido Muncul', '                                    <font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">PT Industri Jamu dan Farmasi Sido Muncul Tbk. adalah perusahaan jamu tradisional dan farmasi dengan menggunakan mesin-mesin mutakhir.</span></font>                                ', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `category_description` text NOT NULL,
  `publication_status` tinyint(4) NOT NULL COMMENT 'Published=1,Unpublished=0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `category_name`, `category_description`, `publication_status`) VALUES
(8, 'Drinks', 'Menyediakan Berbagai Jenis minuman mulai dari Teh, Kopi, Minuman Bersoda, Dll', 1),
(9, 'Snacks', 'Makanan ringan, camilan, atau kudapan(Snacks) merupakan&nbsp;Makanan yang dianggap makanan ringan merupakan makanan untuk menghilangkan rasa lapar seseorang sementara waktu, memberi sedikit pasokan tenaga ke tubuh, atau sesuatu yang dimakan untuk dinikmati rasanya', 1),
(10, 'Groceries', '<font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Groceries adalah bahan makanan sehari-hari, toko pangan, dan toko bahan makanan.</span></font>', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(50) NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `customer_password` varchar(32) NOT NULL,
  `customer_address` text NOT NULL,
  `customer_city` varchar(50) NOT NULL,
  `customer_zipcode` varchar(20) NOT NULL,
  `customer_phone` varchar(20) NOT NULL,
  `customer_country` varchar(100) NOT NULL,
  `customer_active` tinyint(4) NOT NULL COMMENT 'Active=1,Unactive=0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_customer`
--

INSERT INTO `tbl_customer` (`customer_id`, `customer_name`, `customer_email`, `customer_password`, `customer_address`, `customer_city`, `customer_zipcode`, `customer_phone`, `customer_country`, `customer_active`) VALUES
(11, 'cos', 'cos@gmail.com', '4fdf976db93884ce8560c31bb9ce542c', 'jogja', 'jogja', '52151', '084554453223', 'YK', 0),
(12, 'pelanggan', 'pelanggan@gmail.com', '7f78f06d2d1262a0a222ca9834b15d9d', 'jogja', 'jogja', '521513', '084554453224', 'YK', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_option`
--

CREATE TABLE `tbl_option` (
  `option_id` int(11) NOT NULL,
  `site_logo` varchar(100) NOT NULL,
  `site_favicon` varchar(100) NOT NULL,
  `site_copyright` varchar(255) NOT NULL,
  `site_contact_num1` varchar(100) NOT NULL,
  `site_contact_num2` varchar(100) NOT NULL,
  `site_facebook_link` varchar(100) NOT NULL,
  `site_twitter_link` varchar(100) NOT NULL,
  `site_google_plus_link` varchar(100) NOT NULL,
  `site_email_link` varchar(100) NOT NULL,
  `contact_title` varchar(255) NOT NULL,
  `contact_subtitle` varchar(255) NOT NULL,
  `contact_description` text NOT NULL,
  `company_location` varchar(255) NOT NULL,
  `company_number` varchar(100) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_facebook` varchar(100) NOT NULL,
  `company_twitter` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_option`
--

INSERT INTO `tbl_option` (`option_id`, `site_logo`, `site_favicon`, `site_copyright`, `site_contact_num1`, `site_contact_num2`, `site_facebook_link`, `site_twitter_link`, `site_google_plus_link`, `site_email_link`, `contact_title`, `contact_subtitle`, `contact_description`, `company_location`, `company_number`, `company_email`, `company_facebook`, `company_twitter`) VALUES
(1, 'soon_(1).png', 'soon_(1)1.png', 'Irsa Fitrawan Rayes', '085244005724', '085155117669', 'https://www.facebook.com', 'https://www.twitter.com', 'https://www.plus.google.com', 'https://www.gmail.com', 'Irsa Fitrawan Rayes', '085244005724', '                                                                        Admin', '                                    Jalan Suhada No 52 Alas, Sumbawa NTB                                ', '085244005724', 'https://www.gmail.com', 'https://www.facebook.com', 'https://www.twitter.com');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_order`
--

CREATE TABLE `tbl_order` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `order_total` float NOT NULL,
  `actions` varchar(50) NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_order`
--

INSERT INTO `tbl_order` (`order_id`, `customer_id`, `shipping_id`, `payment_id`, `order_total`, `actions`) VALUES
(10, 9, 11, 16, 178250, 'Pending'),
(11, 10, 12, 17, 23862.5, 'Pending'),
(12, 11, 13, 18, 35650, 'Pending'),
(13, 11, 14, 19, 59340, 'Pending'),
(14, 11, 15, 20, 48300, 'Pending'),
(15, 11, 16, 21, 37950, 'Pending'),
(16, 11, 16, 22, 37950, 'Pending'),
(17, 12, 17, 23, 5520, 'Pending'),
(18, 12, 18, 24, 5520, 'Pending');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_order_details`
--

CREATE TABLE `tbl_order_details` (
  `order_details_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_price` float NOT NULL,
  `product_sales_quantity` int(11) NOT NULL,
  `product_image` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_order_details`
--

INSERT INTO `tbl_order_details` (`order_details_id`, `order_id`, `product_id`, `product_name`, `product_price`, `product_sales_quantity`, `product_image`) VALUES
(1, 2, 5, 'Product Five', 10000, 1, NULL),
(2, 3, 5, 'Product Five', 10000, 4, NULL),
(3, 3, 3, 'Product Three', 3500, 3, NULL),
(4, 3, 1, 'Product One', 20000, 1, NULL),
(5, 8, 4, 'Product Four', 350000, 1, 'pic3.jpg'),
(6, 9, 4, 'Product Four', 350000, 1, 'pic3.jpg'),
(7, 10, 6, 'Samsung Galaxy S21 Ultra', 155000, 1, 'sm21u.jpg'),
(8, 11, 2, 'Face Covers 3-Pack', 1250, 1, 'feature-pic2.jpg'),
(9, 11, 1, 'Ultraboost DNA Black Python Shoes', 19500, 1, 'feature-pic1.jpg'),
(10, 12, 14, 'Chitato Sapi Panggang', 11000, 2, 'chitato2.jpg'),
(11, 12, 15, 'Indomilk Coklat', 3000, 3, 'indomilk_cocho1.jpg'),
(12, 13, 9, 'Indomie Goreng', 2400, 4, 'indomie_goreng1.png'),
(13, 13, 13, 'Qtela BBQ', 14000, 3, 'qtela1.jpg'),
(14, 14, 17, 'ABC Kecap', 6000, 7, 'kecap.jpg'),
(15, 16, 14, 'Chitato Sapi Panggang', 11000, 3, 'chitato2.jpg'),
(16, 17, 9, 'Indomie Goreng', 2400, 2, 'indomie_goreng1.png'),
(17, 18, 18, 'Indomie Goreng Rasa Rendang', 2400, 2, 'mie_rendang.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `payment_id` int(11) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `actions` varchar(50) NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_payment`
--

INSERT INTO `tbl_payment` (`payment_id`, `payment_type`, `actions`) VALUES
(1, 'cashon', 'pending'),
(2, 'ssl', 'pending'),
(3, 'cashon', 'pending'),
(4, 'cashon', 'pending'),
(5, 'cashon', 'pending'),
(6, 'cashon', 'pending'),
(7, 'cashon', 'pending'),
(8, 'cashon', 'pending'),
(9, 'cashon', 'pending'),
(10, 'cashon', 'pending'),
(11, 'cashon', 'pending'),
(12, 'cashon', 'pending'),
(13, 'cashon', 'pending'),
(14, 'cashon', 'pending'),
(15, 'cashon', 'pending'),
(16, 'cashon', 'pending'),
(17, 'cashon', 'pending'),
(18, 'cashon', 'pending'),
(19, 'ssl', 'pending'),
(20, 'paypal', 'pending'),
(21, 'paypal', 'pending'),
(22, 'paypal', 'pending'),
(23, 'ssl', 'pending'),
(24, 'paypal', 'pending');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_id` int(11) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_short_description` text NOT NULL,
  `product_long_description` text NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_price` int(11) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `product_feature` tinyint(4) NOT NULL,
  `product_category` int(11) NOT NULL,
  `product_brand` int(11) NOT NULL,
  `product_author` int(11) NOT NULL,
  `product_view` int(11) NOT NULL DEFAULT 0,
  `published_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `product_title`, `product_short_description`, `product_long_description`, `product_image`, `product_price`, `product_quantity`, `product_feature`, `product_category`, `product_brand`, `product_author`, `product_view`, `published_date`, `publication_status`) VALUES
(9, 'Indomie Goreng', '                                                                                                            Indomie Goreng 90 gr', '                                                                                                            <div>Indomie adalah merek produk mi instan dari Indonesia yang diproduksi oleh PT. Indofood CBP Sukses Makmur Tbk</div><div><div class=\"cleditorButton\" title=\"Italic\" style=\"float: left; width: 24px; height: 24px; margin: 1px 0px; background: url(\" ..=\"\" img=\"\" buttons.gif\")=\"\" -24px=\"\" center=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" font-family:=\"\" \"open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\"></div><div class=\"cleditorButton\" title=\"Underline\" style=\"float: left; width: 24px; height: 24px; margin: 1px 0px; background: url(\" ..=\"\" img=\"\" buttons.gif\")=\"\" -48px=\"\" center=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" font-family:=\"\" \"open=\"\" sans\",=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\"></div></div>                                                                                                ', 'indomie_goreng1.png', 2400, 200, 1, 10, 7, 1, 0, '2022-01-13 04:46:27', 1),
(11, 'Sambal ABC', '                                                                                                            <font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">ABC Saus Sambal Asli 135ml</span></font>', '                                                                                                            <div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Saus Sambal ABC Terbukti Kelezatannya&nbsp;</span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Bicara tentang kebiasaan orang Indonesia yang menyantap makanan pedas, di Industri lokal sendiri PT. Heinz ABC Indonesia memperkenalkan berbagai macam produk kecap dan sambal. Namun, yang sudah jelas menjadi andalan orang yang suka pedas yaitu saus sambal ABC, yang sejak dulu terpercaya karena memiliki cita rasa yang khas, lezat, dan memiliki rasa pedas yang maksimal. Bahkan untuk mendukung kepraktisan, maka saus sambal asli ABC hadir dalam kemasan botol atau sachet sehingga Andapun lebih mudah dalam menikmati makanan pedas.</span></font></div>                                                                                                ', 'sambal_abc1.jpg', 6000, 150, 1, 10, 8, 1, 0, '2022-01-13 04:51:36', 1),
(12, 'Tolak Angin', '                                                                                                            <font face=\"Arial, Verdana\"><span style=\"font-size: 10pt;\">Tolak Angin Merupakn Obat Herbal Untuk Masuk Angin</span></font><div style=\"font-family: Arial, Verdana; font-size: 10pt; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;\"><br></div><div style=\"font-family: Arial, Verdana; font-size: 10pt; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;\"><br></div>                                                                                                ', '                                                                                                            <div style=\"\"><span style=\"font-size: 13.3333px; font-family: Arial, Verdana;\">Tolak Angin merupakan Obat Herbal Terstandar (OHT) yang diproduksi di pabrik yang terstandar GMP (Good Manufacturing Product), ISO (International Organization of Standardization), dan HACCP (Hazard Analysis Critical Control Point).</span></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Tolak Angin telah melalui uji toksisitas subkronik dan uji khasiat yang terbukti memelihara/menjaga daya tahan tubuh dengan mengkonsumsi 2 sachet setiap hari selama 7 hari atau lebih.</span></font></div>                                                                                                ', 'angin1.jpg', 15000, 150, 1, 8, 9, 1, 0, '2022-01-13 04:56:57', 1),
(13, 'Qtela BBQ', '                                                                                                            <font face=\"Arial, Verdana\"><span style=\"font-size: 10pt;\">Keripik Singkong Rasa BBQ</span></font>', '                                                                                                            <div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Keripik singkong Qtela terbuat dari singkong pilihan yang diolah secara modern dan higienis serta dipadukan dengan bumbu bumbu berkualitas sehingga menjadikan Qtela sangat renyah dan nikmat.</span></font></div><div style=\"font-family: Arial, Verdana; font-size: 10pt; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;\"><br></div>                                                                                                ', 'qtela1.jpg', 14000, 150, 1, 9, 7, 1, 0, '2022-01-13 05:00:08', 1),
(14, 'Chitato Sapi Panggang', '                                                                                                            <font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">Chitato Rasa Sapi Panggang 68g</span></font>', '                                                                                                            <div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">CHITATO snack potato terbuat dari kentang asli dengan potongan bergelombang yang hadir dalam rasa sapi panggang yang tebal dan kuat untuk memberikan pengalaman baru dalam setiap gigitannya. Keripik kentang Chitato diproses secara higienis dan modern tanpa</span></font></div><div style=\"font-family: Arial, Verdana; font-size: 10pt; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;\"><br></div>                                                                                                ', 'chitato2.jpg', 11000, 150, 1, 9, 7, 1, 0, '2022-01-13 05:02:52', 1),
(15, 'Indomilk Coklat', '                                    Minuman Susu Rasa Coklat                                ', '                                    DIproduksi Oleh PT Indofood Indonesia                                ', 'indomilk_cocho1.jpg', 3000, 150, 1, 8, 7, 1, 0, '2022-01-13 05:22:48', 1),
(16, 'Kuku Bima Ener-G! Rasa Anggur', 'Minuman Ber-energi Rasa Anggur', '<span style=\"color: rgba(49, 53, 59, 0.96); font-family: &quot;open sans&quot;, tahoma, sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);\">KukuBima Ener-G! merupakan minuman energi dengan rasa buah Anggur yang menyegarkan. Bermanfaat untuk meningkatkan stamina bagi pria dan wanita. Sangat baik dikonsumsi sekitar 30 menit sebelum berolahraga, atau beraktivitas. KukuBima Ener-G! Anggur akan memberikan rasa segar pada tubuh.</span>', 'produk-kuku-bima-anggur.png', 5000, 200, 1, 8, 9, 1, 0, '2022-01-13 05:30:01', 1),
(17, 'ABC Kecap', 'ABC Kecap Manis 275ml', '<div style=\"\"><font face=\"Arial, Verdana\"><span style=\"font-size: 13.3333px;\">KECAP ABC - Terbuat dari perasan pertama kedelai pilihan yang memberikan ekstrak rasa dan aroma maksimal, untuk masakan yang kaya rasa</span></font></div><div style=\"font-family: Arial, Verdana; font-size: 10pt; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;\"><br></div>', 'kecap.jpg', 6000, 150, 1, 10, 8, 1, 0, '2022-01-13 05:34:05', 1),
(18, 'Indomie Goreng Rasa Rendang', 'Mie Goreng Rasa Rendang 90gr', 'Indomie adalah merek produk mi instan dari Indonesia yang diproduksi oleh PT. Indofood CBP Sukses Makmur Tbk', 'mie_rendang.jpg', 2400, 150, 1, 10, 7, 1, 0, '2022-01-18 05:20:28', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_shipping`
--

CREATE TABLE `tbl_shipping` (
  `shipping_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `shipping_name` varchar(50) NOT NULL,
  `shipping_email` varchar(100) NOT NULL,
  `shipping_address` text NOT NULL,
  `shipping_city` varchar(100) NOT NULL,
  `shipping_country` varchar(50) NOT NULL,
  `shipping_phone` varchar(20) NOT NULL,
  `shipping_zipcode` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_shipping`
--

INSERT INTO `tbl_shipping` (`shipping_id`, `customer_id`, `shipping_name`, `shipping_email`, `shipping_address`, `shipping_city`, `shipping_country`, `shipping_phone`, `shipping_zipcode`) VALUES
(11, 0, 'Christine', 'christinem@gmail.com', '245 Ralph Street', 'Steyr', 'Austria', '7456320000', '12500'),
(12, 0, 'Bob', 'bob@gmail.com', '3556 Denver Avenue', 'Mira Loma', 'Australia', '7458000025', '3006'),
(13, 0, 'cos', 'cos@gmail.com', 'jogja', 'jogja', 'YK', '084554453223', '52151'),
(14, 0, 'cos', 'cost@gmail.com', 'jogja', 'jogja', 'YK', '084554453223', '52151'),
(15, 0, 'costu', 'costu@gmail.com', 'jogja', 'jogja', 'YK', '084554453223', '52151'),
(16, 0, 'cos', 'costum@gmail.com', 'jogja', 'jogja', 'YK', '084554453223', '52151'),
(17, 0, 'pelanggan', 'pelanggan@gmail.com', 'jogja', 'jogja', 'YK', '084554453224', '521513'),
(18, 0, 'costu', 'pelanggan1@gmail.com', 'jogja', 'jogja', 'YK', '084554453223', '521513');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `slider_id` int(11) NOT NULL,
  `slider_title` varchar(255) NOT NULL,
  `slider_image` varchar(255) NOT NULL,
  `slider_link` varchar(255) NOT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_role` tinyint(4) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `user_email`, `user_password`, `user_role`, `created_time`, `updated_time`) VALUES
(1, 'admin', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 1, '2017-11-13 18:31:36', '2017-11-13 18:31:36'),
(2, 'editor', 'editor@gmail.com', '5aee9dbd2a188839105073571bee1b1f', 2, '2017-11-13 18:31:36', '2017-11-13 18:31:36'),
(3, 'author', 'author@gmail.com', '02bd92faa38aaa6cc0ea75e59937a1ef', 3, '2017-11-13 18:31:36', '2017-11-13 18:31:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`role_id`, `role_name`) VALUES
(1, 'Admin'),
(2, 'Author'),
(3, 'Editor');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_brand`
--
ALTER TABLE `tbl_brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indeks untuk tabel `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indeks untuk tabel `tbl_option`
--
ALTER TABLE `tbl_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Indeks untuk tabel `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indeks untuk tabel `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  ADD PRIMARY KEY (`order_details_id`);

--
-- Indeks untuk tabel `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indeks untuk tabel `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indeks untuk tabel `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  ADD PRIMARY KEY (`shipping_id`);

--
-- Indeks untuk tabel `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`role_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_brand`
--
ALTER TABLE `tbl_brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tbl_option`
--
ALTER TABLE `tbl_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  MODIFY `order_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  MODIFY `shipping_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
